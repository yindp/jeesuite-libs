package com.jeesuite.logging.integrate;

/**
 * 
 * <br>
 * Class Name   : DataActionType
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年8月23日
 */
public enum DataActionType {
   add,update,delete
}
